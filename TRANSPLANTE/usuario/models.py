
#-*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from proyectos.models import Proyecto
from TRANSPLANTE import settings

class UserProfile(models.Model):
    #REQUIRED_FIELDS = ('user',)
    #USERNAME_FIELD = 'user'
    #is_anonymous = models.BooleanField(default=False)
    #is_authenticated = models.BooleanField(default=False)

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    direccion = models.CharField(max_length=50, default='')
    telefono = models.CharField(max_length=50, default='')
    captrabajo = models.IntegerField(default=0)
    trabasignado = models.FloatField(default=0)
    descripcion = models.CharField(max_length=240, default='')
    es_administrador = models.BooleanField(default=False)
    proyectos = models.ManyToManyField(Proyecto, default=None)


    def nombre_valido(self,nombre):
        users = UserProfile.objects.all()
        for u in users:
            if u.user.username == nombre:
                return False
        return True

