

from django.shortcuts import redirect
from django.template import RequestContext
from django.shortcuts import render_to_response, render
from login.forms import loginform
from django.contrib.auth import authenticate, login, logout


#------------------------permisos

from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
#--------modelos de las aplicaciones
from proyectos.models import Proyecto
from usuario.models import UserProfile

#print "------------------prueba permisos\n"




administrador, a1 = Group.objects.get_or_create(name='administrador')
ct_proyecto = ContentType.objects.get_for_model(Proyecto)
permiso, p = Permission.objects.get_or_create(codename='admin', name='es administrador', content_type=ct_proyecto)



def login_page(request):

    message = None
    if request.method == 'POST':
        form=loginform(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username,password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    print(user.id)
                    try:
                        up = UserProfile.objects.get(user_id=user.id)
                    except UserProfile.DoesNotExist:
                        up = None
                    if up.es_administrador ==True:
                        user.user_permissions.permissions.set(permiso)
                    return redirect('homepage')
                else:
                    form = loginform()
                    message = "Usuario inactivo"
                    context = {'form': form, 'message': message}

            else:
                form = loginform()
                message = 'Datos incorrectos'
                context = {'form': form, 'message': message}

    else:
        form = loginform()
        context = {'form': form, 'message': message}

    return render(request, 'login.html', context)



def homepage(request):

    up = User.objects.get(username=request.user)
    user = UserProfile.objects.get(user=up)
    if user.es_administrador ==True:
        up.user_permissions = [permiso]
    return render(request, 'index.html')


def salir(request):

    logout(request)
    return redirect('login')


