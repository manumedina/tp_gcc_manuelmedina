from django.forms import ModelForm
from django import forms
from flujos.models import Daily

class DailyForm(forms.ModelForm):
    """
        Clase para realizar comentarios dentro de un proyecto
    """

    class Meta:
        model = Daily
        fields = ('text','author','Horas',)