# TP_GCC_ManuelMedina

- El trabajo seleccionado es un trabajo del cual yo (Manuel Medina) 
y otros dos compañeros: Romina Riveros y Enzo Unzaín lo hemos realizado
en la materia de Ing. de Software II

- El trabajo fue levantado a un repositorio nuevo en GitLab

- Se creo el archivo .gitlab-ci.yml para la integración continua con tres
entornos, test, staging y production para la automatización para el testeo, 
y en caso de estar todo ok posteriormente desplegarlo en Heroku  

- Actualmente el automatizador, luego de testear, no logra seguir con el 
entorno de staging para luego deployarlo en production en Heroku